import React from "react";
import Contact from "./components/Contact";
import Form from "./components/Form";
import Navbar from "./components/Navbar";
import Swal from "sweetalert2";
import { ToastContainer, toast } from "react-toastify";
import { v5 as uuidv5 } from "uuid";
import "react-toastify/dist/ReactToastify.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Axios from "axios";

class App extends React.Component {
  state = {
    // name:"nabin",
    // user:{
    //   id:1,
    //   rollno:23
    // }

    contact: []
    // showHide: true
  };

  // handleClick = () => {
  //   this.setState({ showHide: !this.state.showHide });
  //   // console.log("Called");
  // };

  handleDelete = id => {
    let filterData = this.state.contact.filter(function(contact) {
      return contact.id !== id;
    });

    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!"
    }).then(result => {
      if (result.value) {
        Axios.delete(`http://localhost:5000/api/v1/contacts/${id}`)
          .then(response => {
            if (response.status === 200) {
              this.getContact();
            }
          })
          .catch(response => {
            console.log(response);
          });
        toast.success("Successfully Deleted !!!");
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
      }
    });
  };

  handleSubmmitedData = data => {
    // // let id = this.state.contact.length + 1;
    // let insertData = {
    //   id: uuidv5(),
    //   ...data
    // };
    // console.log(insertData);
    // this.setState({
    //   contact: [insertData, ...this.state.contact]
    // });
    // toast.success("Data Inserted");
    Axios.post("http://localhost:5000/api/v1/contacts", data)
      .then(response => {
        console.log(response);
        if (response.status === 201) {
          this.getContact();
        }
      })
      .catch(response => {
        console.log(response);
      });
  };

  handleEditData = editData => {
    // let editContact = this.state.contact.map(function(contact) {
    //   if (editData.id === contact.id) {
    //     return editData;
    //   }
    //   return contact;
    // });
    // this.setState({
    //   contact: editContact
    // });
    Axios.put(`http://localhost:5000/api/v1/contacts/${editData.id}`, editData)
      .then(response => {
        console.log(response);
        if (response.status === 200) {
          this.getContact();
        }
      })
      .catch(err => {
        console.error(err);
      });
    toast.success("Edit Successfully");
  };
  componentDidMount() {
    this.getContact();
  }

  getContact = () => {
    Axios.get("http://localhost:5000/api/v1/contacts")
      .then(response => {
        this.setState({ contact: response.data.data });
      })
      .catch(err => {
        console.log(err);
      });
  };
  render() {
    // this is objects
    // let person =[
    //   {name:"nabin", rollno:23, age:24},
    //   {name:"nabinstha", rollno:23, age:24},
    //   {name:"nabin1", rollno:23, age:24}

    // ]
    // let data = "hello";
    // console.log(this.state.name)

    // console.log(this.state.contact);
    // for(let i=0; i<this.state.contact.length; i++){
    //   console.log(this.state.contact[i])
    // }

    return (
      <div>
        {" "}
        {/* <Navbar data="I am from navbar" age={24} user ={this.state.user}/> */}{" "}
        {/* <button type="button" onClick={this.handleClick}>
                  Show
                </button> */}{" "}
        <Navbar title="Contact Management System" />
        <Form submitted={this.handleSubmmitedData} />{" "}
        {this.state.contact.map(contact => (
          <Contact
            contact={contact}
            delete={this.handleDelete}
            edit={this.handleEditData}
            key={contact._id}
          />
        ))}{" "}
        <ToastContainer />
      </div>
    );
  }
}
export default App;
