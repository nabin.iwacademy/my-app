import React from "react";

class Form extends React.Component {
  constructor() {
    super();
    this.state = {
      name: "",
      email: "",
      phone: "",
      error: {}
    };
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubbmit = e => {
    e.preventDefault();
    const { name, email, phone } = this.state;
    if (name === "") {
      return this.setState({ error: { name: "Please enter your Name" } });
    } else if (email === "") {
      return this.setState({ error: { email: "Please Enter Your email" } });
    } else if (phone === "") {
      return this.setState({
        error: { phone: "Please enter Your Phone Number" }
      });
    }
    this.props.submitted(this.state);
    this.setState({ error: {}, name: "", email: "", phone: "" });
  };
  render() {
    const { error } = this.state;
    return (
      <div className="card w-50 mx-auto mt-5">
        <div
          className="card-header"
          style={{ backgroundColor: "blue", color: "#fff" }}
        >
          <h2>Contact Form</h2>
        </div>
        <div className="card-body">
          <form className="form-group" onSubmit={this.handleSubbmit}>
            <label htmlFor="name">Name</label>
            <input
              type="text"
              placeholder="Name"
              className="form-control"
              onChange={this.handleChange}
              value={this.state.name}
              name="name"
            ></input>
            <span style={{ color: "red" }}>{error.name}</span>
            <label htmlFor="email">email</label>
            <input
              type="email"
              placeholder="email"
              className="form-control"
              value={this.state.email}
              name="email"
              onChange={this.handleChange}
            ></input>
            <span style={{ color: "red" }}>{error.email}</span>
            <label htmlFor="phone">phone</label>
            <input
              type="number"
              placeholder="phone"
              className="form-control"
              onChange={this.handleChange}
              value={this.state.phone}
              name="phone"
            ></input>
            <span style={{ color: "red" }}>{error.phone}</span>
            <button type="submit" className="btn btn-primary mt-3 btn-block ">
              Submit
            </button>
          </form>
        </div>
      </div>
    );
  }
}
export default Form;
