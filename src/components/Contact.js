import React from "react";

class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowing: false,
      isEditing: false,
      name: this.props.contact.name,
      email: this.props.contact.email,
      phone: this.props.contact.phone,
      error: {}
    };
  }

  handleShowHide = () => {
    this.setState({
      isShowing: !this.state.isShowing
    });
  };

  handleEditing = () => {
    this.setState({
      isEditing: true
    });
  };

  handleDelete = () => {
    this.props.delete(this.props.contact._id);
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleSubbmit = e => {
    e.preventDefault();
    const { name, email, phone } = this.state;
    if (name === "") {
      return this.setState({
        error: {
          name: "Please enter your Name"
        }
      });
    } else if (email === "") {
      return this.setState({
        error: {
          email: "Please Enter Your email"
        }
      });
    } else if (phone === "") {
      return this.setState({
        error: {
          phone: "Please enter Your Phone Number"
        }
      });
    }
    let editedData = {
      name,
      email,
      phone,
      id: this.props.contact._id
    };
    this.props.edit(editedData);
    this.setState({
      error: {},
      isEditing: false
    });
  };
  render() {
    let cls = this.state.isShowing ? "fas fa-sort-up" : "fas fa-sort-down";
    const { error } = this.state;
    if (this.state.isEditing) {
      return (
        <div className="card w-50 mx-auto mt-5">
          <div
            className="card-header"
            style={{
              backgroundColor: "blue",
              color: "#fff"
            }}
          >
            <h2> Edit Contact Form </h2>{" "}
          </div>{" "}
          <div className="card-body">
            <form className="form-group" onSubmit={this.handleSubbmit}>
              <label htmlFor="name"> Name </label>{" "}
              <input
                type="text"
                placeholder="Name"
                className="form-control"
                onChange={this.handleChange}
                value={this.state.name}
                name="name"
              ></input>{" "}
              <span> {error.name} </span> <label htmlFor="email"> email </label>{" "}
              <input
                type="email"
                placeholder="email"
                className="form-control"
                value={this.state.email}
                name="email"
                onChange={this.handleChange}
              ></input>{" "}
              <span> {error.email} </span>{" "}
              <label htmlFor="phone"> phone </label>{" "}
              <input
                type="number"
                placeholder="phone"
                className="form-control"
                onChange={this.handleChange}
                value={this.state.phone}
                name="phone"
              ></input>{" "}
              <span> {error.phone} </span>{" "}
              <button type="submit" className="btn btn-primary mt-3 btn-block ">
                Submit{" "}
              </button>{" "}
            </form>{" "}
          </div>{" "}
        </div>
      );
    } else {
      return (
        <div>
          <div className="card w-50 mt-5 mx-auto">
            <div
              className="card-header "
              style={{
                backgroundColor: "blue",
                color: "#fff",
                fontWeight: "bold"
              }}
            >
              <h2>
                <i className={cls} onClick={this.handleShowHide}>
                  {" "}
                </i>{" "}
                {this.props.contact.name}{" "}
                <div className="float-right">
                  <i
                    className="fas fa-trash mr-3"
                    onClick={this.handleDelete}
                  ></i>{" "}
                  <i className="fas fa-edit" onClick={this.handleEditing}>
                    {" "}
                  </i>{" "}
                </div>{" "}
              </h2>{" "}
            </div>{" "}
            {this.state.isShowing ? (
              <div className="card-body">
                <ul className="list-group">
                  <li className="list-group-item">
                    Email: {this.props.contact.email}{" "}
                  </li>{" "}
                  <li className="list-group-item">
                    Phone: {this.props.contact.phone}{" "}
                  </li>{" "}
                </ul>{" "}
              </div>
            ) : null}{" "}
          </div>{" "}
        </div>
      );
    }
  }
}

export default Contact;
