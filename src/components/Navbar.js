import React from "react";
// done previously
// class Navbar extends React.Component{
//     render(){
//         console.log(this.props)
//         // this.props.data = 0;
//         return (<div>
//             <h1>{this.props.data}</h1>
//             <h1>{this.props.age}</h1>

//             </div>);
//     }
// }

class Navbar extends React.Component {
  render() {
    // console.log(this.props);
    // this.props.data = 0;
    return (
      <nav
        className="navbar navbar-expand-lg navbar-light"
        style={{
          backgroundColor: "blue",
          color: "white"
        }}
      >
        <h1
          className="navbar-brand"
          style={{
            color: "white"
          }}
        >
          {" "}
          {this.props.title}{" "}
        </h1>{" "}
      </nav>
    );
  }
}
export default Navbar;
